#!/bin/bash

for s in *
do
	echo "[Linking] $s"
	ln -s "$(pwd)/$s" ~/.local/bin/$s
done
echo "Done!"
