# Automated Video Editing

Obs: English isn't my first language, so please let me know if I made any mistakes or if there's an easier way to express the ideas.

I wrote this script to help me automate some video editing tasks, like doing cuts on scene changes.
I included a test video (Battlefield 4 gameplay), so you can test the script on it.

Obs: Make sure to check out the **Dependencies** section before trying it.

## How it works

1. It uses scene-detect (by NapoleonWils0n, check the Dependencies section) to detect when the difference between 2 following frames is greater than a defined threshold.
2. Then, it saves the time of the detected frames to a file.
3. It uses scene-images to extract the frames from the file.
4. It generates a preview video with the extracted images.
5. It mounts the Melt Framework command with the "-split" flags being the content of the file.
6. It generates a Melt XML file, with the original video and audio, both with all the defined cuts from the file.
7. It adds Kdenlive-specific XML stuff to the Melt XML file, so you can load it in Kdenlive, to finish editing the video (it displays a compatibility alert, when first loading the project, but it works).

## Functionality Overview

- Loop through all videos in current directory
	- Make a directory with video name (without extension); cd to it
	- Loop through all threshold values defined in threshold_values
		- Make directory for the current threshold; cd to it
			- Run scene-detect on the video (generates a list of detection points and saves it to the file defined in detection_filename). Detects the points by checking the difference between the frames; when its equal or above the defined threshold, it saves the specific time to the detection file
			- Run scene-images, to save the detection points as images, so you can preview where the program is going to split the video
			- Use the images to generate a preview.mp4 video
			- Generate a XML Melt file with MELT Framework
			- Add Kdenlive-specific XML stuff to the existing XML, so you can load it on Kdenlive
			- Go to previous directory
		- Loop until all threshold values have been processed

## Options

```
-t	Threshold values. Example: -t "0.1 0.5 0.9"
-l	Detection limit, the max detection points allowed. Example: -l 500
-a	Add audio to the project, like a background music. Example: -a audio.mp3
-s	Split audio the same way the video was split
-h	Help message
```

## Usage

You can control it by using the defined flags. Some examples:

- Generate multiple threshold values (so you can decide on which one is better to edit):
```
./generate.sh -t "0.1 0.5 0.9"
```

- Add background music (or any other additional audio):
```
./generate.sh -a music.mp3
```

- Add additional audio and replicate the cuts made to the video to this additional audio:
```
./generate.sh -a audio.mp3 -s
```

- Limit the max amount of cuts, so the script ignores the threshold values that produces a number of detection points above the defined limit (500 means: the max amount of cuts is 500):

```
./generate.sh -t "0.1 0.5 0.9" -l 500
```

## Environment

I wrote this script on Zorin OS, an Ubuntu based distro.

Info about my system/computer:

```
OS: Zorin OS 16.1 x86_64 
Host: Inspiron 7559 1.3.0 
Kernel: 5.15.0-43-generic 
Shell: bash 5.0.17 
DE: GNOME 
WM: Mutter 
Terminal: gnome-terminal 
CPU: Intel i7-6700HQ (8) @ 3.500GHz 
GPU: Intel HD Graphics 530 
GPU: NVIDIA GeForce GTX 960M 
Memory: 4122MiB / 15869MiB 
```

## Dependencies

- ### FFMPEG

It uses FFMPEG to detect scenes, extract frames and etc...

```
sudo apt install ffmpeg
```

- ### Melt Framework

It uses Melt Framework to pre-edit the video and generate the Kdenlive projects, so you can edit it on Kdenlive after the process.

```
sudo apt install melt
```

- ### FFMPEG Scripts by NapoleonWils0n

It uses a set of scripts made by NapoleonWils0n. You can find the original here: [https://github.com/NapoleonWils0n/ffmpeg-scripts](https://github.com/NapoleonWils0n/ffmpeg-scripts)

It's located in the "Dependencies" directory.

## Observations

This is the first time I open source a script. So, any info and tips about licenses, open source and etc, are welcome.

## Contact

Any doubts, you can contact me by sending a message to this e-mail: darwin.brandao@gmail.com

Please, use the following text in the e-mail title, so I know it's related to this project: [GitLab: Video Pre-Editing Script]

