#!/bin/bash

# ================
# === OVERVIEW ===
# ================

#- Loop through all videos in current directory
#	- Make a directory with video name (without extension); cd to it
#	- Loop through all threshold values defined in threshold_values
#		- Make directory for the current threshold; cd to it
#			- Run scene-detect on the video (generates a list of detection points and saves it to the file defined in detection_filename). Detects the points by checking the difference between the frames; when its equal or above the defined threshold, it saves the specific time to the detection file
#			- Run scene-images, to save the detection points as images, so you can preview where the program is going to split the video
#			- Use the images to generate a preview.mp4 video
#			- Generate a XML Melt file with MELT Framework
#			- Add Kdenlive-specific XML stuff to the existing XML, so you can load it on Kdenlive
#			- Go to previous directory
#		- Loop until all threshold values have been processed

#==========================================================================================================================

# =============
# === TODOs ===
# =============

# TODO - Support for custom melt commands within args (ex: generate -m "melt intro.mp4 in=0 out=5 video1.mkv in=5 out=10").
# TODO - Preview video shows images out of order. Make it show them ir order.
# TODO - Make it work with other shells (ZShell, Fish, etc)
# TODO - Finish additional audio related stuff

#==========================================================================================================================

# =================================
# === Dependencies Verification ===
# =================================

if ! [ $(command -v ffmpeg) ]; then
       echo "Error: FFmpeg not installed."
       exit 1
fi

if ! [ $(command -v melt) ]; then
	echo "Error: Melt Framework not installed."
	exit 1
fi

#==========================================================================================================================

# Variables
threshold_values=(0.1 0.5 0.7 0.9)
detection_limit=100
detection_filename=detection.txt
audio=""
split_audio=false

# Directory of the script, not the working directory
DIR=$( cd -- "$( dirname -- "$(readlink ${BASH_SOURCE[0]})" )" &> /dev/null && pwd | awk '{ gsub(/[[:space:]]/, "\\ ", $0); print }' )

#==========================================================================================================================

# ==================================
# === KDENLIVE XML PROJECT STUFF ===
# ==================================

# Kdenlive modifies the default MLT file, generated by MELT. These are the XML structures Kdenlive uses to recognize valid .kdenlive files and properly open them within the editor.
# The {NUMBERS} are replaced with data from the MLT file generated by the MELT command.

# Some references:
# https://invent.kde.org/multimedia/kdenlive/-/blob/master/dev-docs/mlt-intro.md
# https://www.mltframework.org/docs/melt/
# https://www.mltframework.org/docs/mltxml/

# https://serverfault.com/a/72511
read -r -d '' kdenlive_xml <<"EOF"
  <playlist id="main_bin">
    <property name="xml_retain">1</property>
{0}
  </playlist>
  <playlist id="playlist1"/>
  <playlist id="playlist2">
{1}
  </playlist>
  <playlist id="playlist3"/>
  <tractor id="tractor0" in="0" out="12183">
    <property name="kdenlive:audio_track">1</property>
    <property name="kdenlive:trackheight">67</property>
    <property name="kdenlive:timeline_active">1</property>
    <track hide="video" producer="playlist0"/>
    <track hide="video" producer="playlist1"/>
  </tractor>
  <tractor id="tractor1" in="00:00:00.000" out="{2}">
    <property name="kdenlive:trackheight">67</property>
    <property name="kdenlive:timeline_active"/>
    <property name="kdenlive:thumbs_format"/>
    <property name="kdenlive:audio_rec"/>
    <track producer="playlist2"/>
    <track producer="playlist3"/>
  </tractor>
  <tractor id="tractor2" in="00:00:00.000" out="{3}">
    <track producer="tractor0"/>
    <track producer="tractor1"/>
  </tractor>
EOF

#==========================================================================================================================

# ===============
# === OPTIONS ===
# ===============

helpMsg()
{
helpMsg=""
read -r -d '' helpMsg <<"EOF"
-t \t Threshold values. Example: -t "0.1 0.5 0.9"
-l \t Detection limit, the max detection points allowed. Example: -l 500
-a \t Add audio to the project, like a background music. Example: -a audio.mp3
-s \t Split audio the same way the video was split
-h \t Help message
EOF
echo -e $"$helpMsg"
}

# Loop flags (https://linuxconfig.org/how-to-use-getopts-to-parse-a-script-options)
while getopts t:l:hm:a:s flag; do
	case $flag in
		# Treshold Values
		t) 
			# Delete array values
			unset threshold_values
			
			# Set variable with user input args
			threshold_values=$OPTARG;;
			
		# Detection Limit
		l) 
			detection_limit=$OPTARG;;
			
		# Help Message
		h)
			helpMsg 
			exit 1;;
		
		# Melt Framework additional parameters
		m)
			params="$OPTARG";;
		
		# Add audio to the project
		a)
			audio="$OPTARG";;
		
		# Split audio at the same frames the video was split	
		s)
			split_audio=true;;
			
		# Unknown Flag
		?) 
			echo "$OPTARG is not a valid flag."
			helpMsg
			exit 1
	esac
done

#==========================================================================================================================

echo "[Threshold Values] $threshold_values"
echo "[Detection Limit] $detection_limit"

# =======================
# === PRE-EDIT VIDEOS ===
# =======================

# Loop all videos in the directory
for video in *.{mkv,mp4}
do
	# Check if video file exists
	if [ -f "$video" ]
	then
		echo -e "[-------------] Current video: $video [-------------]\n"

		# Removes extension from video filename
		video_name="${video%.*}" # https://stackoverflow.com/a/12152997
		
		# Create folder for holding video project files
		mkdir "$video_name"

		# Go to new folder
		cd "$video_name"

		# Loop threshold values
		for t in ${threshold_values[@]}
		do
			# =======================
			# === SCENE DETECTION ===
			# =======================
			
			echo "[Current Threshold] $t"

			# Create folder
			mkdir "$t"

			# Copy raw video to the new folder
			cp "../$video" "$t/$video"

			# Go to new folder
			cd $t

			# Run scene-detect (script em ~/.local/bin) (not anymore, it's in Dependencies/ffmpeg-scripts-master/)
			eval ${DIR}/Dependencies/ffmpeg-scripts-master/scene-detect -i "$video" -t $t -o $detection_filename # Avoid using "-f sec", because scene-images is expecting hh:mm:ss.fff

			# Count for detection points
			detection_points=$(wc -l < $detection_filename)

			# If detection count is greater than detection_limit, ignore and go to next iteration
			if [ $detection_points -gt $detection_limit ]
			then
				echo -e "\n[-------------]Number of detection points($detection_points) above limit($detection_limit)[-------------]\n"
				cd ..
				continue
			fi
			
			#==========================================================================================================================
			
			# ========================
			# === GENERATE PREVIEW ===
			# ========================

			# Save frame of each minute/second in detection.txt
			eval ${DIR}/Dependencies/ffmpeg-scripts-master/scene-images -i "$video" -c $detection_filename

			# Generate preview video (X seconds per scene)
			images=""
			count=0
			interval=30
			for p in *.png
			do
				# Check if images exists
				if [ -f "$p" ]
				then
					images="$images \"$p\" in=$count out=$((count+$interval))"
					count=$((count+$interval))
				fi
			done

			# Check if images exists
			if [ -z "$images" ] # Check if $images it's empty
			then
				echo "Could not find images for MELT to process..."
			else
				# Generate video scenes
				echo "[+] Generating Preview: melt $images -consumer avformat:preview.mp4"
				eval melt $images -consumer avformat:preview.mp4 progressive=1 width=1920 height=1080
			fi
			
			#==========================================================================================================================
			
			# ===============================================
			# === CONVERT DETECTION POINTS TO MELT SPLITS ===
			# ===============================================

			# Split video in the detection.txt points
			echo -e "\n[+] Editing video...\n"

			splits=""
			detection_content=$(cat $detection_filename)

			# Get video framerate
			frame_rate=$(echo "$(ffprobe -v 0 -of compact=p=0 -select_streams 0 -show_entries stream=r_frame_rate "$video")" | awk -F '[/=]' '{ print $2 }')

			# Last cut
			last_cut_frame=0

			# Mount MELT query splits from detection.txt
			while IFS= read -r line; do

				echo "[Kdenlive Split] hh:mm:ss.fff: $line"

			    	# Convert hh:mm:ss.fff to seconds
			    	seconds=$(echo "$line" | awk -F: '{ print ($1 * 3600) + ($2 * 60) + $3 }')

				echo "[Kdenlive Split] Seconds: $seconds"

				# Check if it's not the first frame
				if [ $seconds != "0" ]
				then

			    	# Convert from seconds to frames
			    	frames=$(echo "scale=2;$seconds*$frame_rate" | bc) # scale is the max decimal positions we want in the answer

					echo "[Kdenlive Split] Frame: $frames"

					# Interval relative to last cut frame (-split parameter is relative to last cut)
					interval=$(echo "$frames-$last_cut_frame" | bc)

					echo "[Kdenlive Split] Interval: $interval"

					# Mount splits section of MELT command
			    	splits="$splits -split $interval "

					last_cut_frame=$frames

				else
					echo "[++] Skipping first frame..."
				fi

			done <<< "$detection_content"
			
			#==========================================================================================================================

			# echo -e "\n[Final MELT Command] melt \"$video\" $splits -consumer xml:\"$video_name.mlt\" root=\"$(pwd)\" fps=$frame_rate"
			
			# ==============================
			# === GENERATE MELT XML FILE ===
			# ==============================
			
			if [ $audio != "" ]
			then
				if [  "$split_audio" = true ]
				then
					# Project with video, original audio, additional audio, everything split
					melt "$video" $splits -audio-track "$audio" $splits -consumer xml:"$video_name.kdenlive" root="$(pwd)" fps=$frame_rate
				else
					# Project with video, original audio, additional audio, only original video and audio split
					melt "$video" $splits -audio-track "$audio" -consumer xml:"$video_name.kdenlive" root="$(pwd)" fps=$frame_rate
				fi
			else
				# Project with video, original audio, both split, without any additional audio
				melt "$video" $splits -consumer xml:"$video_name.kdenlive" root="$(pwd)" fps=$frame_rate
			fi

			echo -e "\n[Converting from .MLT to .Kdenlive]\n"

			project="$video_name.kdenlive"
			
			#==========================================================================================================================
			
			# ============================================
			# === CONVERT MELT XML TO KDENLIVE PROJECT ===
			# ============================================
			
			sed 's/LC_NUMERIC="pt_BR.UTF-8"/& producer="main_bin"/' -i "$project" # Insert producer="main_bin" into <mlt> params after LC_NUMERIC="pt_BR.UTF-8"
			sed -e '/tractor/d' -e '/track/d' -i "$project" # Remove tractor0 and its tracks
			sed -e 's/version="6.20.0"/version="7.3.0"/' -e '/^[[:space:]]*$/d' -i "$project" # Update project version

			playlist_entries="$(awk -F "playlist0\">|</playlist>" '{for(i=2;i<=NF;i+=2){print $i}}' RS="" "$project" | sed '/^[[:space:]]*$/d')" # Get <entry> tags within playlist0
			echo "$playlist_entries"
			first_entry="$(echo "$playlist_entries" | tail -n 1)" # Get first <entry>
			echo "$first_entry"
			video_length=$(echo "$first_entry" |  cut -d '"' -f6) # Get value from out="value"
			echo "$video_length"

			# Make a copy of the project file (awk couldn't save the text to the same file)
			cp "$project" PreEdited.kdenlive
			
			# Insert kdenlive_xml content on the line before </mlt>, then remove empty lines https://stackoverflow.com/a/2512379
			awk -vf2="$kdenlive_xml" '/\/mlt/{print f2;print;next}1' "$project" > "PreEdited.kdenlive"
			
			# Delete project file, now we have the PreEdited.kdenlive
			rm "$project"

			sed "s|{0}|$first_entry|" -i "PreEdited.kdenlive"
			sed "s|{1}|$(echo $playlist_entries)|" -i "PreEdited.kdenlive"
			sed "s/{2}/$video_length/" -i "PreEdited.kdenlive"
			sed "s/{3}/$video_length/" -i "PreEdited.kdenlive"
			
			#==========================================================================================================================

			# Go back to root folder
			cd ..

			echo -e "\n[-------------]Finished video processing for $t threshold value.[-------------]\n"
		done
	fi
	cd ..
done
echo "\n[-------------] Finished [-------------]\n"
